import conexion from "./conexion.js";
import { rejects } from "assert";

var alumnosDb = {}
alumnosDb.insertar = function insertar(alumno){

    return new Promise((resolve, rejects)=>{
        let sqlConsulta = "Insert into alumnos set ?";
        conexion.query(sqlConsulta, alumno, function(err,res){
            
            if(err){
                console.log("Surgio un error al insertar", err.message);
                rejects(err);

            }
            else{
                const alumno = {
                    id:res.id,
                }
                resolve(alumno);
            }
        });
    });
}

alumnosDb.mostrarTodos = function mostrarTodos() {
    return new Promise((resolve, rejects)=>{
        var sqlConsulta = "Select * from alumnos";
        conexion.query(sqlConsulta, null, function(err, res){
            if(err){
                console.log("Surgio un error al mostrar todo" + err);
                rejects(err);
            }
            else{
                resolve(res);
            }
        });
    });
}

export default alumnosDb;