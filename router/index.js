import express from "express";
import json from "body-parser";
import conexion from "../models/conexion.js";
import alumnosDb from "../models/alumnos.js";
export const router = express.Router();

let rows;


router.get('/', (req, res)=>{
    res.render('index', {titulo:"Mi primer pagina EJS", nombre: "Yair Alejandro Ontiveros Govea"});

    router.get('/tabla', (req,res)=>{
        const params = {
            numero : req.query.numero
        };
        res.render('tabla', params);
    });

    router.post('/tabla', (req,res)=>{
        const params = {
            numero : req.body.numero
        }
        res.render('tabla', params);
    });

    router.get('/cotizacion', (req,res)=>{
        const params = {
            numFolio : req.query.numFolio,
            descripcion : req.query.descripcion,

            precio : req.query.precio,

            porcentaje : req.query.porcentaje,



        };
        res.render('cotizacion', params);
    });

    router.post('/cotizacion', (req,res)=>{
        const params = {
            numFolio : req.body.numFolio,
            descripcion : req.body.descripcion,

            precio : req.body.precio,

            porcentaje : req.body.porcentaje,
        }
        res.render('cotizacion', params);
    });

    
    router.get('/alumnos', async(req,res)=>{
        rows = await alumnosDb.mostrarTodos();
        res.render('alumnos', {reg:rows});
    });

    let params;
    

    router.post('/alumnos', async(req,res)=>{
        try{
            params = {
                matricula:req.body.matricula,
                nombre:req.body.nombre,
                domicilio:req.body.domicilio,
                sexo:req.body.sexo,
                especialidad:req.body.especialidad
            
            };
        const registros = await alumnosDb.insertar(params);
        console.log("------------- registros " + registros);

        }catch(error){
            console.error(error)
            res.status(400).send("sucedio un error: "+ error);
        }
        rows = await alumnosDb.mostrarTodos();
        res.render('alumnos', {reg:rows});

    });

    //prueba();
});

async function prueba(){
    try{
        const res = await conexion.execute("select * from alumnos");
        console.log("El resultado es ", res);
    } catch (error){
        console.log("Hubo un error", error);
    }
    finally{
        conexion.end();
    }
}




export default { router }